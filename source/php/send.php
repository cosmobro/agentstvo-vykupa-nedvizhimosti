<?php
/*
     ___           ___           ___                       ___
    /  /\         /  /\         /  /\          ___        /__/\
   /  /::\       /  /::\       /  /::\        /  /\       \  \:\
  /  /:/\:\     /  /:/\:\     /  /:/\:\      /  /:/        \__\:\
 /  /:/  \:\   /  /:/~/::\   /  /:/~/:/     /  /:/     ___ /  /::\
/__/:/ \__\:\ /__/:/ /:/\:\ /__/:/ /:/___  /  /::\    /__/\  /:/\:\
\  \:\ /  /:/ \  \:\/:/__\/ \  \:\/:::::/ /__/:/\:\   \  \:\/:/__\/
 \  \:\  /:/   \  \::/       \  \::/~~~~  \__\/  \:\   \  \::/
  \  \:\/:/     \  \:\        \  \:\           \  \:\   \  \:\
   \  \::/       \  \:\        \  \:\           \__\/    \  \:\
    \__\/         \__\/         \__\/                     \__\/

                          Orlov Artur
                    email: orlovarth@ya.ru
                       skype: orlovarth
                   vk: https://vk.com/oarth
*/

require('config.php');
require('lib/PHPMailer/PHPMailerAutoload.php');


if (!isset($_POST['phone'])) {
  die('Вы не ввели свой номер телефона!');
}

$mail = new PHPMailer;

if ($smtpUse) {
  // Настройка SMTP
  $mail->isSMTP();
  $mail->SMTPDebug = 0;
  $mail->Host = $smtpHost;
  $mail->SMTPAuth = true;
  $mail->Username = $smtpUser;
  $mail->Password = $smtpPass;
  $mail->SMTPSecure = $smtpSecure;
  $mail->Port = $smtpPort;
}

// Настройка параметров письма
$mail->CharSet = 'utf-8';
$mail->setFrom($senderAddress, $senderName, 0);
$mail->isHTML(true);
$mail->Subject = $subject;
$mail->Body = $body;
$mail->AltBody = $altBody;


if (isset($_POST['mailto'])) {
  // Добавление получателей для конкретной формы
  foreach ($_POST['mailto'] as $key => $val) {
    $mail->AddAddress(trim($val));
  }
} else {
  // Добавление глобальных получателей
  foreach ($recipients as $recipient) {
    $mail->addAddress($recipient);
  }
}

// Заполнение письма
foreach ($fields as $k => $v) {
  if (isset($_POST[$k])) {
    $mail->Body .= "<b>$v:</b> " . $_POST[$k] . "<br>";
    $mail->AltBody .= "$v: " . $_POST[$k] . "/n";
  }
}

// Добавление времени
date_default_timezone_set('Europe/Moscow');
$mail->Body .= "<b>Дата:</b> " . date("m.d.y H:i:s") . "<br>";
$mail->AltBody .= "Дата: " . date("m.d.y H:i:s") . "/n";

if (!$mail->send()) {
   echo 'Ошибка: ' . $mail->ErrorInfo;
  header('HTTP/1.1 500 Internal Server Error');
//  die();
} else {
  echo 'Заявка успешно отправлена.';
  exit();
}
?>
