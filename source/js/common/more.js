'use strict';

function init() {
  var $buttons = $('.js-more__button');

  function moreToggle(e) {
    e.preventDefault();

    var $this = $(this);
    var buttonShowText = $this.data('show-text');
    var buttonHideText = $this.data('hide-text');
    var $toScroll = $($this.data('scroll'));
    var $container = $this.closest('.js-more__container');
    var targetName = $this.attr('href');
    var $target = $container.find(targetName);

    $target.toggleClass('active');
    $this.toggleClass('active').text($target.hasClass('active') ? buttonHideText : buttonShowText);

    if (!$this.hasClass('active')) {
      $('html, body').scrollTop($toScroll.offset().top);
    }
  }

  $buttons.click(moreToggle)
}

module.exports = init;