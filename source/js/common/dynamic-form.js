'use strict';

function main() {
  var $form = $('.js-dynamic-form');

  if (!$form.length) {
    return;
  }

  var $select = $form.find('.js-dynamic-form__select');
  var $tabs = $form.find('.js-dynamic-form__tab');

  function changeTab() {
    var target = $select.find('option:selected').data('tab');
    var $targetTab = $(target);

    $tabs.filter(':not(' + target + ')').hide();
    $targetTab.show();
  }

  $select.change(changeTab);
}

module.exports = main;