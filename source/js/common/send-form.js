'use strict';

function sendForm() {
  var $inputs = $('input');
  var $steps = $('.js-steps');
  var validationRules = {
    name: {
      minlength: 3,
      required: true,
      notPlaceholder: true
    },
    phone: {
      phoneTest: true,
      phoneRequired: true,
      required: true,
      notPlaceholder: true
    }
  };
  var validationMessages = {
    name: {
      minlength: 'Минимум 3 символа',
      required: 'Введите имя',
      notPlaceholder: 'Введите имя'
    },
    phone: {
      phoneTest: 'Некорректный телефон',
      phoneRequired: 'Введите телефон',
      required: 'Введите телефон',
      notPlaceholder: 'Введите телефон'
    }
  };
  var stepsValidationRules = {
    cost: {
      required: true,
      notPlaceholder: true
    },
    type: {
      required: true
    },
    destination: {
      required: true
    },
    address: {
      required: true,
      notPlaceholder: true
    },
    rooms: {
      required: true,
      notPlaceholder: true
    },
    area: {
      required: true,
      notPlaceholder: true
    },
    sectorArea: {
      required: true,
      notPlaceholder: true
    },
    phone: {
      phoneTest: true,
      phoneRequired: true,
      required: true,
      notPlaceholder: true
    }
  };
  var stepsValidationMessages = {
    cost: {
      required: 'Введите стоимость',
      notPlaceholder: 'Введите стоимость'
    },
    type: {
      required: 'Выберите тип'
    },
    destination: {
      required: 'Выберите назначение'
    },
    address: {
      required: 'Введите адрес',
      notPlaceholder: 'Введите адрес'
    },
    rooms: {
      required: 'Введите кол-во комнат',
      notPlaceholder: 'Введите кол-во комнат'
    },
    area: {
      required: 'Введите площадь',
      notPlaceholder: 'Введите площадь'
    },
    sectorArea: {
      required: 'Введите площадь участка',
      notPlaceholder: 'Введите площадь участка'
    },
    phone: {
      phoneTest: 'Некорректный телефон',
      phoneRequired: 'Введите телефон',
      required: 'Введите телефон',
      notPlaceholder: 'Введите телефон'
    }
  };
  var $thanksPopup = $('#popup-thanks');
  var $errorPopup = $('#popup-error');
  var timeToClose = 7000;
  var popupSettings = {
    items: {
      src: null
    },
    type: 'inline',
    midClick: true,
    showCloseBtn: false,
    removalDelay: 200,
    mainClass: 'mfp-fade',
    fixedContentPos: true
  };
  // Таймер закрытия окна
  window.timerToCloseMsgPopup = null;

  // Ввод по маске
  $('input[type="tel"]').mask("+7 999 999 99 99");
  $inputs.placeholder();

  // Методы валидатора
  // Проверка на подсказку, т.к. фикс плейсхолдера конфликтует с валидатором
  $.validator.addMethod('notPlaceholder', function (val, el) {
    return this.optional(el) || (val !== $(el).attr('placeholder'));
  }, $.validator.messages.required);

  // Проверка на телефон
  $.validator.addMethod('phoneTest', function (val, el) {
    if (val == '+7 ___ ___ __ __' || val == '' || val == $(el).attr('placeholder')) {
      return true;
    }
    return /^\+7\s\d{3,3}\s\d{3,3}\s\d{2,2}\s\d{2,2}$/i.test(val);
  }, $.validator.messages.required);

  // Проверка на обязательность телефона
  $.validator.addMethod('phoneRequired', function (val, el) {
    return val == '+7 ___ ___ __ __' && $(el).closest('form').hasClass('isTry2Submit') ? false : true;
  }, $.validator.messages.required);

  // Показыватель ошибок
  function validationShowErrorsHandler(errorMap, errorList) {
    var submitButton = $(this.currentForm).find('button, input:submit');
    for (var error in errorList) {
      var element = $(errorList[error].element),
          errorMessage = errorList[error].message;
      element.val(errorMessage).addClass('error').attr('placeholder', errorMessage);
    }
  }

  // Запихиватель ошибок в плейсхолдер
  function validationErrorPlacementHandler(error, element) {
    element.attr("placeholder", error[0].outerText);
  }

  // Открывает попап
  function openPopup(popup) {
    popupSettings.items.src = popup;
    $.magnificPopup.open(popupSettings, 0);
  }

  // Ставит таймер закрытия окна
  function setClosePopupTimer() {
    window.timerToCloseMsgPopup = setTimeout(function () {
      $.magnificPopup.close();
    }, timeToClose);
  }

  function sendForm(data, form) {
    var $form = $(form);
    var goal = $form.data('goal');

    // Форма отправляется
    $form.addClass('sending');

    // Обработчик успеха отправки
    function submitSuccessHandler(data) {
      yaCounter40566755.reachGoal(goal);
      yaCounter40566755.reachGoal('submit-form-any');
      ga('send', 'event', 'form', 'submit', goal);
      ga('send', 'event', 'form', 'submit', 'any');

      openPopup($thanksPopup);

      setClosePopupTimer();
    }

    // Обработчик ошибки отправки
    function submitErrorHandler(data) {
      openPopup($errorPopup);

      setClosePopupTimer();
    }

    // Обработчик завершения отправки
    function submitCompleteHandler() {
      // Форма отправилась
      $form.removeClass('sending');
    }

    $.ajax({
      type: 'POST',
      url: 'php/send.php',
      data: data,
      success: submitSuccessHandler,
      error: submitErrorHandler,
      complete: submitCompleteHandler
    });
  }

  // Обработчик отправки формы
  function submitHandler(form) {
    sendForm($(form).serialize(), form);
  }

  function nextStep(step) {
    var $stepsNavs = $('.js-steps__nav');
    var $stepsTabs = $('.js-steps__tab');

    $stepsNavs.removeClass('active').filter('[data-rel="' + step + '"]').addClass('active');
    $stepsTabs.removeClass('active').filter(step).addClass('active');
  }

  function stepsSend($form) {
    var data = '';
    var $inputs = $steps.find('input, select');

    $('.js-steps__form').each(function () {
      var $this = $(this);

      data += $this.serialize() + '&';
    });

    data = data.replace(/\w*=&/gi, '');

    console.log(data);
    
    sendForm(data, $form);
  }

  // Обработчик перехода на следующий шаг
  function stepsNextHandler(form) {
    var $form = $(form);

    if ($form.hasClass('js-steps__form_last')) {

      stepsSend($form);

      return false;
    }

    var step = $(form).data('rel');


    nextStep(step);

    return false;
  }

  // Валидация отправки
  $('.js-form__ajax').each(function () {
    $(this).validate({
      rules: validationRules,
      messages: validationMessages,
      errorPlacement: validationErrorPlacementHandler,
      showErrors: validationShowErrorsHandler,
      submitHandler: submitHandler,
      errorClass: 'error',
      validClass: 'valid',
      errorElement: 'span',
      onkeyup: false
    });
  });

  // Валидация шагов
  $('.js-steps__form').each(function () {
    $(this).validate({
      rules: stepsValidationRules,
      messages: stepsValidationMessages,
      errorPlacement: validationErrorPlacementHandler,
      showErrors: validationShowErrorsHandler,
      submitHandler: stepsNextHandler,
      errorClass: 'error',
      validClass: 'valid',
      errorElement: 'span',
      onkeyup: false
    });
  });

  $inputs.on('focus', function () {
    if ($(this).hasClass('error')) {
      $(this).val('').removeClass('error');
    }
  });

  $('button').click(function () {
    $(this).closest('form').addClass('isTry2Submit');
  });
}

module.exports = sendForm;