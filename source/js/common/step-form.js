'use strict';

function init() {
  var $forms = $('.js-steps__form');
  var $tabs = $('.js-tab');
  var $navs = $('.js-steps__nav');


  // Валидация
  $forms.each(function () {
    $(this).validate({
      rules: validationRules,
      messages: validationMessages,
      errorPlacement: validationErrorPlacementHandler,
      showErrors: validationShowErrorsHandler,
      submitHandler: submitHandler,
      errorClass: 'error',
      validClass: 'valid',
      errorElement: 'span',
      onkeyup: false
    });
  });
}

module.exports = init;