'use strict';

function init() {
  var menu = $('.js-fixable');
  var navoffset = /*$('.js-fixable__open')*/menu.offset().top;

  if ($(this).scrollTop() > navoffset) {
    menu.addClass("fixed");
  } else {
    menu.removeClass("fixed");
  }

  $(window).scroll(function () {
    if ($(this).scrollTop() > navoffset) {
      menu.addClass("fixed");
    } else {
      menu.removeClass("fixed");
    }
  });
}

module.exports = init;