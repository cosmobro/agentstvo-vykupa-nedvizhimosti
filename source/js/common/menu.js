'use strict';

function init() {
  var $menu = $('.js-menu');
  var $menuButton = $menu.find('.js-menu__button');
  var $hideButton = $('.js-menu__hide');

  function hideMenu(e) {
    $menu.removeClass('active');
  }

  function toggleMenu(e) {
    e.preventDefault();

    $menu.toggleClass('active');
  }


  $menuButton.click(toggleMenu);
  $hideButton.click(hideMenu);
}

module.exports = init;