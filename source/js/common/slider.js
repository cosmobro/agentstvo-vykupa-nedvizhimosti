'use strict';

function init() {
  var $slider = $('.js-slider');
  var $sliderTrack = $slider.find('.js-slider__list');
  var $nextButton = $slider.find('.js-slider__nav_next');
  var $prevButton = $slider.find('.js-slider__nav_prev');

  var sliderOptions = {
    prevArrow: $prevButton,
    nextArrow: $nextButton,
    variableWidth: true,
    centerMode: true,
    slidesToShow: 1
  };

  $sliderTrack.slick(sliderOptions);
}

module.exports = init;