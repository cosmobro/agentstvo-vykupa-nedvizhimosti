'use strict';

var menu = require('./common/menu');
var slider = require('./common/slider');
var map = require('./common/map');
var more = require('./common/more');
var popups = require('./common/popups');
var scrollTo = require('./common/scroll-to');
var fixedMenu = require('./common/fixed-menu');
var sendForm = require('./common/send-form');
var stepForm = require('./common/step-form');
var dynamicForm = require('./common/dynamic-form');

$(function () {
  menu();
  slider();
  map.init();
  more();
  popups();
  scrollTo();
  fixedMenu();
  sendForm();
  dynamicForm();
  // stepForm();
});

