'use strict';

module.exports = function () {
  $.gulp.task('inline:styles', function () {
    return $.gulp.src([$.config.root + '/*.html', $.config.root + '/*.php'])
      .pipe($.gp.inlineSource())
      .pipe($.gp.replace('url(\'../', 'url(\'assets/'))
      .pipe($.gp.replace('url(../', 'url(assets/'))
      .pipe($.gulp.dest($.config.root));
  })
};