const pngquant = require('imagemin-pngquant');

module.exports = {
  root: './build',

  autoprefixerConfig: ['last 3 version', '> 1%', 'ie 8', 'ie 9', 'Opera 12.1'],

  imageminOptions: {
    progressive: true,
    use: [pngquant()],
    interlaced: true,
    multipass: true
  },

  dataPath: './source/template/data/data.json'
};